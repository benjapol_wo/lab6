package wordcounter;

import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import java.util.Scanner;

/**
 * Entry point of the program. Will count the occurrence(s) of each word from
 * srcText with WordCounter. Then will print out the first 20 words sorted
 * alphabetically and the 20 most occurred words.
 * 
 * @author Benjapol Worakan 5710546577
 * @version 15.2.25
 */
public class WordCount {

	/**
	 * Main method, entry point of the program. Will print out words from
	 * srcText sorted alphabetically and by occurrence(s).
	 * 
	 * @param args
	 *            - not used
	 * @throws IOException
	 *             when unable to read from srcText's InputStream.
	 */
	public static void main(String[] args) throws IOException {
		WordCounter wordCounter = new WordCounter();
		Scanner srcText = new Scanner(
				new URL(
						"https://bitbucket.org/skeoop/oop/raw/master/week6/Alice-in-Wonderland.txt")
						.openStream());
		srcText.useDelimiter("[\\s,.\\?!\"():;]+");

		while (srcText.hasNext()) {
			wordCounter.addWord(srcText.next());
		}

		System.out.println("First twenty words sorted alphabetically:");
		String[] sortedWords = wordCounter.getSortedWords();

		for (int i = 0; i < Math.min(sortedWords.length, 20); i++) {
			System.out.printf("%3d. | %20s | %3d times\n", i + 1,
					sortedWords[i], wordCounter.getCount(sortedWords[i]));
		}

		System.out.println();
		System.out.println("Twenty most occurred words:");

		for (int i = 0; i < sortedWords.length; i++) {
			sortedWords[i] = String.format("%03d %s",
					wordCounter.getCount(sortedWords[i]), sortedWords[i]);
		}

		Arrays.sort(sortedWords);

		for (int i = sortedWords.length - 1, j = 0; i >= 0 && j < 20; i--, j++) {
			System.out.printf("%3d. | %20s | %3d times\n", j + 1,
					sortedWords[i].substring(4),
					Integer.parseInt(sortedWords[i].substring(0, 3)));
		}

		srcText.close();
	}
}
