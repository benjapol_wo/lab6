package wordcounter;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Set;

/**
 * A WordCounter that will count the occurrence(s) of each word by storing its
 * occurrence(s) each String phrase as a value in a HashMap.
 * 
 * @author Benjapol Worakan 5710546577
 * @version 15.2.25
 */
public class WordCounter {

	/** A map containing words as keys and occurrence of each word as values. */
	private HashMap<String, Integer> words;

	/**
	 * Constructor that will initiate the HashMap to be key-value pairs of
	 * Strings and Integers.
	 */
	public WordCounter() {
		words = new HashMap<String, Integer>();
	}

	/**
	 * Add the word occurrence to the HashMap. Will put the word to the HashMap
	 * if it's new, or replace its value if it's already occurred.
	 * 
	 * @param word
	 *            - a word to be added to the HashMap
	 */
	public void addWord(String word) {
		word = word.toLowerCase();
		if (words.get(word) == null) {
			words.put(word, 1);
		} else {
			words.replace(word, words.get(word) + 1);
		}
	}

	/**
	 * Get the Set of words that are in the HashMap.
	 * 
	 * @return a Set of words that are in the HashMap.
	 */
	public Set<String> getWords() {
		return words.keySet();
	}

	/**
	 * Get the count of occurrence(s) of the specified word.
	 * 
	 * @param word
	 *            - a phrase (String) to get its occurrence(s) count
	 * @return the count of word's occurrence(s).
	 */
	public int getCount(String word) {
		if (words.containsKey(word)) {
			return words.get(word);
		}
		return 0;
	}

	/**
	 * Get the words in the HashMap as an array of String, sorted.
	 * 
	 * @return the words in the HashMap as an array of String (sorted)
	 */
	public String[] getSortedWords() {
		String[] returnArray = words.keySet().toArray(new String[0]);
		Arrays.sort(returnArray);
		return returnArray;
	}
}
